--Build the query to generate a report about the most significant customers (which have maximum sales) through various sales channels. 
--The 5 largest customers are required for each channel.
--Column sales_percentage shows percentage of customerís sales within channel sales

select
	channel_desc ,
	cust_id ,
	cust_last_name ,
	cust_first_name ,
	to_char( amount_sold , '99999.99') , --make format like we need 
	to_char(sales_percentage_ * 100, 'FM9.99999') || ' %' as sales_percentage
from
	(
	select
		c2.channel_desc ,
		c.cust_id ,
		c.cust_last_name ,
		c.cust_first_name ,
		sum(s.amount_sold) as amount_sold,
		rank() over (partition by c2.channel_desc --rank to take only 5 customers
	order by
		sum(s.amount_sold) desc ) as rank_in_line ,
		sum(s.amount_sold)/ sum(sum(s.amount_sold)) over(partition by c2.channel_desc) as sales_percentage_
	from
		sh.customers c
	join sh.sales s on
		s.cust_id = c.cust_id
	join sh.channels c2 on
		c2.channel_id = s.channel_id
	group by
		c2.channel_desc ,
		c.cust_id ,
		c.cust_first_name ,
		c.cust_last_name
	order by
		sum(s.amount_sold) desc 
) tab
where
	rank_in_line <= 5;


--Compose query to retrieve data for report with sales totals for all products in Photo category in 
--Asia (use data for 2000 year). Calculate report total 
--(YEAR_SUM).



select
	p.prod_name,
	case
		when (sum(s.amount_sold) filter(
	where
		t.calendar_quarter_number = 1)) is null then 0 -- case if no prod sales for quarter 
		else sum(s.amount_sold) filter(					-- filter to  take values from certain quarter
	where
		t.calendar_quarter_number = 1)
	end as q1,
	case
		when(sum(s.amount_sold) filter(
	where
		t.calendar_quarter_number = 2)) is null then 0
		else sum(s.amount_sold) filter(
	where
		t.calendar_quarter_number = 2 as q2,
		case
			when (sum(s.amount_sold) filter(
		where
			t.calendar_quarter_number = 3)) is null then 0
			else sum(s.amount_sold) filter(
		where
			t.calendar_quarter_number = 3)
		end as q3,
		case
			when sum(s.amount_sold) filter(
		where
			t.calendar_quarter_number = 4) is null then 0
			else sum(s.amount_sold) filter(
		where
			t.calendar_quarter_number = 4)
		end as q4,
		sum(s.amount_sold) as year_sum
	from
		sh.sales s
	join sh.customers c on
		c.cust_id = s.cust_id
	join sh.countries c2 on
		c2.country_id = c.country_id
	join sh.products p on
		p.prod_id = s.prod_id
	join sh.times t on
		t.time_id = s.time_id
	where
		c2.country_subregion = 'Asia'
		and t.calendar_year = 2000
		and p.prod_category = 'Photo'
	group by
		p.prod_id
	order by
		p.prod_name ;



select
	channel_desc ,
	to_char(cust_id, '9999999') as cust_id,
	cust_last_name ,
	cust_first_name,
	to_char(amount_sold_ , '99999.00') as amount_sold
from
	(
	select
		tb.amount_sold_
,
		tb.channel_desc
,
		tb.cust_id
,
		c2.cust_last_name 
,
		c2.cust_first_name 
,
		(case
			when sum_1998 is not null then dense_rank() over (partition by tb.channel_desc 
		order by
			sum_1998 desc nulls last )
		end ) as rank_1998
,
		(case
			when sum_1998 is not null then dense_rank() over (partition by tb.channel_desc
		order by
			sum_1999 desc nulls last )
		end ) as rank_1999
,
		(case
			when sum_1998 is not null then dense_rank() over (partition by tb.channel_desc -- dense rank if customers have the same sum
		order by
			sum_2001 desc nulls last )
		end ) as rank_2001
	from
		(
		select
			distinct s.cust_id ,
			c.channel_desc ,
			sum(s.amount_sold) filter (
			where t.calendar_year = 1998) over w as sum_1998
,
			sum(s.amount_sold) filter (
			where t.calendar_year = 1999) over w as sum_1999
,
			sum(s.amount_sold) filter (
			where t.calendar_year = 2001) over w as sum_2001
,
			sum(s.amount_sold) over w as amount_sold_
		from
			sh.sales s
		join sh.channels c on
			c.channel_id = s.channel_id
		join sh.times t on
			t.time_id = s.time_id
		where
			t.calendar_year = 1998
			or t.calendar_year = 1999
			or t.calendar_year = 2001
window w as (partition by s.cust_id ,
			c.channel_desc)) as tb
	inner join sh.customers c2 on
		c2.cust_id = tb.cust_id) as tbb
where
	rank_1998 <= 300
	and rank_1999 <= 300
	and rank_2001 <= 300
order by
	amount_sold_ desc
;


--
--Build the query to generate a report about customers who were included into TOP 300 
--(based on the amount of sales) in 1998, 1999 and 2001. This 
--report should separate clients by sales channels, and, at the same time, 
--channels should be calculated independently (i.e. only purchases made on 
--selected channel are relevant).



--Build the query to generate the report about sales in America and Europe:
select
	t.calendar_month_desc ,
	p.prod_category ,
	to_char(sum(sum(s.amount_sold) filter(where c2.country_region like 'Americas')) over(partition by t.calendar_month_desc ,
	p.prod_category
order by
	p.prod_category ) , '999,999') as "Americas SALES",
	to_char(sum(sum(s.amount_sold) filter(where c2.country_region like 'Europe')) over(partition by t.calendar_month_desc , -- partition makes for month on prod 
	p.prod_category
order by
	sum(s.amount_sold) ) , '999,999') as "Europe SALES"
from
	sh.sales s
join sh.products p on
	p.prod_id = s.prod_id
join sh.customers c on
	c.cust_id = s.cust_id
join sh.countries c2 on
	c.country_id = c2.country_id
join sh.times t on
	t.time_id = s.time_id
where
	t.calendar_month_desc like '2000-01'
	or t.calendar_month_desc like '2000-02'
	or t.calendar_month_desc like '2000-03'
group by
	t.calendar_month_desc ,
	p.prod_category;




