--Figure out what security precautions are already used in your 'dvd_rental' database; -- send description
--Password security
--The password-based authentication methods are md5 and password. These methods operate similarly except for the way that the password is sent across the connection, namely MD5-hashed and clear-text respectively.
--
--If you are at all concerned about password "sniffing" attacks then md5 is preferred. Plain password 
--should always be avoided if possible. However, md5 cannot be used with the db_user_namespace feature. If the connection is protected by SSL encryption then password can be used safely 
--(though SSL certificate authentication might be a better choice if one is depending on using SSL).
--
--PostgreSQL database passwords are separate from operating system user passwords. The password for each 
--database user is stored in the pg_authid system catalog. Passwords can be managed with the SQL commands 
--CREATE USER and ALTER ROLE, e.g., CREATE USER foo WITH PASSWORD 'secret'. 
--If no password has been set up for a user, the stored password is null and password authentication will always fail for that user.


--2. Implement role-based authentication model for dvd_rental database:
--� Create group roles: DB developer, backend tester (read-only), customer (read-only for film and actor)
--� Create personalized role for any customer already existing in the dvd_rental database. Role name 
--must be client_{first_name}_{last_name} 
--(omit curly brackets). Customer's payment and rental history must not be empty.
--� Assign proper privileges to each role.
--� Verify that all roles are working as intended.

select
	*
from
	pg_catalog.pg_roles ;

create role db_developer;

grant all on
all tables in schema public to db_developer;

create role backend_tester1 LOGIN password '11' inherit;

grant
select
	on
	all tables in schema public to backend_tester1;

--now like backend_tester1
insert
	into
	film(title)
values ('Some');

--Error occurred during SQL query execution
--�������:
 --SQL Error [42501]: ������: ��� ������� � ������� film

--select
--	*
--from
--	information_schema.table_privileges tp
--where
--	grantee like 'customer1';

--now like customer1

insert
	into
	film(title)
values ('Some');

--Error occurred during SQL query execution
--�������:
 --SQL Error [42501]: ������: ��� ������� � ������� film

create role customer1 LOGIN password '12' inherit ;

grant
select
	on
	table film ,
	actor to customer1;

create role client_mary_smith;

--3. Read about row-level security (https://www.postgresql.org/docs/12/ddl-rowsecurity.html) 
--and configure it for your database, so that the 
--customer can only access his own data in "rental" and "payment" tables (verify using the personalized role you previously created).

alter table rental enable row level security;

alter table payment enable row level security;

create policy fp_s on
payment to client_mary_smith
	using (payment_id = (
select
	(p.payment_id)
from
	customer c
join payment p on
	p.customer_id = c.customer_id
where
	c.customer_id = 1 ));

create policy fp_ss on
rental to client_mary_smith
	using (rental_id = (
select
	(r.rental_id)
from
	customer c
join rental r on
	r.customer_id = c.customer_id
where
	c.customer_id = 1));


--4. Prepare answers to the following questions:

--4.1 How can one restrict access to certain columns of a database table?
--by row-level security:
--
--alter table rental enable row level security;
--create policy fp_ss on
--rental to client_mary_smith
--	using (...);


--4.2 What is the difference between user identification and user authentication?

--Authentication is a process of validating an identity. 
--In computing, this generally means verifying that a user or entity is who they say they are.

--While authentication is concerned with validating identity, authorization focuses on controlling what capabilities 
--are associated with those identities or accounts. 
--Once you know who someone is, the authorization functionality determines what they can do.

--4.3 What are the recommended authentication protocols for PostgreSQL?
--Trust Authentication. ...
--Password Authentication. ...
--GSSAPI Authentication. ...
--SSPI Authentication. ...
--Kerberos Authentication. ...
--Ident Authentication. ...
--Peer Authentication. ...
--LDAP Authentication.

--4.4 What is proxy authentication in PostgreSQL and what is it for? 
--Why does it make the previously discussed role-based access control easier to 
--implement?
--The Cloud SQL Auth proxy provides secure access to your 
--instances without the need for Authorized networks or for configuring SSL.
--Its easier because of: 
--Secure connections: The Cloud SQL Auth proxy automatically encrypts traffic to and from the database using TLS 1.2 
--with a 128-bit AES cipher; SSL certificates are used to verify client and server identities.
--Easier connection management: The Cloud SQL Auth proxy handles authentication with Cloud SQL, 
--removing the need to provide static IP addresses.

