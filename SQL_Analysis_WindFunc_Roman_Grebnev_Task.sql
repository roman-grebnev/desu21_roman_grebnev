

--select c.channel_desc  
--,c3.country_region  
--,max(count(s.quantity_sold)) over (partition by c3.country_region , c.channel_desc ) as sales
--,(max(count(s.quantity_sold)) over (partition by c3.country_region , c.channel_desc ))*100/
--sum(count(s.quantity_sold)) over (partition by  c.channel_desc ) as all_sum
--from   sh.sales s 
--join sh.channels c on c.channel_id = s.channel_id 
--join sh.customers c2 on c2.cust_id = s.cust_id 
--join sh.countries c3 on c3.country_id = c2.country_id
--group by  c.channel_desc  , c3.country_region
--order by c.channel_desc , all_sum ;

select channel_desc, country_region , sales , to_char(sales_ , '99.99') || '%' as "SALES %" from (select c.channel_desc  
,c3.country_region  
, to_char(max(count(s.quantity_sold)) over (partition by c3.country_region , c.channel_desc ) ,'9999999.00' )  as sales
,(max(count(s.quantity_sold)) over (partition by c3.country_region , c.channel_desc ))*100/
(sum(count(s.quantity_sold)) over (partition by  c.channel_desc ))  as SALES_
from sh.sales s 
join sh.channels c on c.channel_id = s.channel_id 
join sh.customers c2 on c2.cust_id = s.cust_id 
join sh.countries c3 on c3.country_id = c2.country_id
group by  c.channel_desc  , c3.country_region
order by SALES_ desc limit 4 ) as t
order by sales desc ;


--select channel_desc , country_region , max(count_quan) over (partition by channel_desc ) from(select c.channel_desc  
--,c3.country_region , count(s.quantity_sold) as count_quan  from   sh.sales s 
--join sh.channels c on c.channel_id = s.channel_id 
--join sh.customers c2 on c2.cust_id = s.cust_id 
--join sh.countries c3 on c3.country_id = c2.country_id
--group by  c.channel_desc  , c3.country_region) as t
--group by channel_desc , country_region , t.count_quan;
--
--select  c.channel_desc , c3.country_region , count(s.quantity_sold)  from sh.sales s 
--join sh.channels c on c.channel_id = s.channel_id 
--join sh.customers c2 on c2.cust_id = s.cust_id 
--join sh.countries c3 on c3.country_id = c2.country_id
--group by c.channel_desc , c3.country_region
--having count(s.quantity_sold) in(select max(all_res.tot_c) from(select count(s.quantity_sold) as tot_c , c3.country_region , c.channel_desc from sh.sales s2 
--join sh.channels c4 on c4.channel_id = s2.channel_id 
--join sh.customers c5 on c5.cust_id = s2.cust_id 
--join sh.countries c6 on c6.country_id = c5.country_id
--group by c3.country_region , c.channel_desc 
--order by count(s.quantity_sold)
--) as all_res
--join sh.countries c7 on c7.country_region  = all_res.country_region
--group by c.channel_desc , c7.country_region
--);






