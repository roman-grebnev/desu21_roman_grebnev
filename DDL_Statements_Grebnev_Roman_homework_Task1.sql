drop table kinder_schema.individual_edu_plan cascade

create table kinder_schema.children 
(
	child_id serial primary key ,
	first_name varchar(30) not null,
	last_name varchar(30) not null ,
	patronymic varchar(30) not null,
	birth_date date not null check(((extract (year
from
current_date )) - (extract(year
from
birth_date) )) < 8 ),
	group_id int not null references kinder_schema.child_groups (group_id),
	order_id int not null references kinder_schema.orders (order_id)
)

create table kinder_schema.orders 
(
	order_id serial primary key ,
	order_number int not null,
	order_date date default current_date
)

create table kinder_schema.child_groups
(
	group_id serial primary key,
	group_number integer not null,
	group_name varchar(30) not null,
	room_number integer not null
)
create table kinder_schema.buildings
(
	building_id serial primary key,
	capacity int not null ,
	build_name varchar(30) not null 
)

create table kinder_schema.positions
(
	position_id serial primary key,
	pos_name varchar(30) unique not null,
	salary int 
)

create table kinder_schema.programs
(
	program_id serial primary key ,
	program_name varchar(30) unique not null,
	program_type varchar(30) not null,
	registration_date date default current_date 
)
create table kinder_schema.employees
(
	employee_id serial primary key,
	first_name varchar(30) not null ,
	second_name varchar(30) not null,
	patronymic varchar(30) not null,
	position_id int not null references kinder_schema.positions (position_id),
	group_id int not null references kinder_schema.child_groups (group_id),
	building_id int not null references kinder_schema.buildings (building_id)
)

create table kinder_schema.sections 
(
	section_id serial primary key,
	section_name varchar(30) unique not null,
	employee_id int not null references kinder_schema.employees (employee_id)
)
create table kinder_schema.individual_edu_plan
(
	child_id int not null references kinder_schema.children (child_id),
	section_id int not null references kinder_schema.sections (section_id)
)
create table kinder_schema.edu_plan
(
	group_id int not null references kinder_schema.child_groups (group_id),
	program_id int not null references kinder_schema.programs (program_id)
)
 



insert
	into
	kinder_schema.children (first_name,
	last_name ,
	patronymic ,
	birth_date ,
	group_id ,
	order_id )
values ('Ivan',
'Ivanov',
'Ivanovich' ,
'2016-01-08' ,
1 ,
1);

insert
	into
	kinder_schema.children (first_name,
	last_name ,
	patronymic ,
	birth_date ,
	group_id ,
	order_id )
values ('Petr',
'Petrov',
'Petrovich' ,
'2018-01-02' ,
2 ,
3);

insert
	into
	kinder_schema.child_groups (group_number ,
	group_name ,
	room_number)
values ('1',
'Adults' ,
2);

insert
	into
	kinder_schema.child_groups (group_number ,
	group_name ,
	room_number)
values ('21',
'Middle age kids' ,
3);

insert
	into
	kinder_schema.orders (order_number)
values ('1');

insert
	into
	kinder_schema.orders (order_number)
values ('2');

insert
	into
	kinder_schema.buildings (capacity,
	build_name)
values(300 ,
'Main building' ),
	(200,
'Canteen');

insert
	into
	kinder_schema.positions (pos_name ,
	salary)
values ('Educator',
300),
	   ('Medic' ,
500);

insert
	into
	kinder_schema.positions (pos_name ,
	salary)
values('Cook' ,
400)
	  
insert
	into
	kinder_schema.employees (first_name ,
	second_name ,
	patronymic ,
	position_id ,
	group_id ,
	building_id)
values ('Oleg' ,
'Olegov' ,
'Olegovich' ,
3 ,
2 ,
2 ),
	   ('Natalia' ,
'Natalieva' ,
'Olegovna' ,
1 ,
1 ,
1);

insert
	into
	kinder_schema.programs (program_name ,
	program_type)
values ('Math',
'Educational'),
	   ('Logic' ,
'Educational');

insert
	into
	kinder_schema.sections (section_name ,
	employee_id)
values('Painting' ,
1),
	  ( 'Math' ,
1);

insert
	into
	kinder_schema.edu_plan (group_id ,
	program_id)
values (1,
1),
		(2,
2);

insert
	into
	kinder_schema.individual_edu_plan (child_id ,
	section_id)
values (10,
1),
		(2,
2);

alter table kinder_schema.buildings 
add record_ts date not null default current_date;

alter table kinder_schema.child_groups 
add record_ts date not null default current_date;

alter table kinder_schema.edu_plan 
add record_ts date not null default current_date;

alter table kinder_schema.employees 
add record_ts date not null default current_date;

alter table kinder_schema.individual_edu_plan 
add record_ts date not null default current_date;

alter table kinder_schema.orders 
add record_ts date not null default current_date;

alter table kinder_schema.positions 
add record_ts date not null default current_date;

alter table kinder_schema.programs 
add record_ts date not null default current_date;

alter table kinder_schema.sections 
add record_ts date not null default current_date;

alter table kinder_schema.children 
add record_ts date not null default current_date;








