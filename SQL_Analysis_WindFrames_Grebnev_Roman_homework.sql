
--1. Analyze annual sales by channels and regions. Build the query to generate the same report

select
	country_region ,
	calendar_year ,
	channel_desc ,
	channel_current
,
	to_char(round (100 * channel_current / current_region , 2 ) , '99.99 %') as "% BY CHANNELS"
,
	to_char(round(100 * channel_sales_prev / region_previous , 2 ), '99.99 %') as "% PREVIOUS PERIOD"
,
	to_char(round(100 *(channel_current / current_region - channel_sales_prev / region_previous) , 2), '99.99 %') as "% DIFF"
from
	(
	select
		c2.country_region ,
		c3.channel_desc ,
		t.calendar_year ,
		sum(s.amount_sold) as channel_current
,
		first_value(sum(s.amount_sold)) over(partition by c2.country_region ,
		c3.channel_desc
	order by
		t.calendar_year 
groups between 1 preceding and 1 preceding) as channel_sales_prev,
		sum(sum(s.amount_sold)) over(partition by c2.country_region ,
		t.calendar_year) as current_region,
		sum(sum(s.amount_sold)) over(partition by c2.country_region
	order by
		t.calendar_year
groups between 1 preceding and 1 preceding) as region_previous
	from
		sh.sales s
	left join sh.times t on
		t.time_id = s.time_id
	left join sh.customers c on
		c.cust_id = s.cust_id
	left join sh.countries c2 on
		c2.country_id = c.country_id
	left join sh.channels c3 on
		c3.channel_id = s.channel_id
	where
		c2.country_region in('Americas' , 'Asia', 'Europe')
	group by
		c2.country_region ,
		c3.channel_desc ,
		t.calendar_year
) tab
where
	calendar_year between 1999 and 2001
order by
	country_region ,
	calendar_year,
	channel_desc;

--2. Build the query to generate a sales report for the 49th, 50th and 51st weeks of 1999. Add column CUM_SUM for accumulated amounts within 
--weeks. For each day, display the average sales for the previous, current and next days (centered moving average, CENTERED_3_DAY_AVG column). 
--For Monday, calculate average weekend sales + Monday + Tuesday. For Friday, calculate the average sales for Thursday + Friday + weekends.


select
	calendar_week_number ,
	time_id,
	day_name ,
	to_char(sales , '999999999.99') as sales ,
	to_char(cum_sum , '999999999.99') as cum_sum,
	to_char(round(centered_3days_avg , 2),'99999999.99') as  centered_3_day_avg
from
	(
	select
		t.calendar_week_number ,
		t.time_id ,
		t.day_name ,
		sum(s.amount_sold) as sales,
		sum(sum(s.amount_sold)) over (partition by t.calendar_week_number
	order by
		t.time_id 
rows between unbounded preceding and current row ) as cum_sum,
		case
			when t.day_name in ('Sunday')
then t.time_id - interval '1 day'
			else t.time_id
		end  as f
,
		avg(sum(s.amount_sold)) over (
		order by
		case
			when t.day_name in ('Sunday') 
			then t.time_id - interval '1 day'
			else t.time_id
		end groups between 1 preceding and 1 following ) as st
,
		case
			when t.day_name in ('Monday')
then avg(sum(s.amount_sold)) over (
			order by t.time_id 
groups between current row and 1 following)
			when t.day_name in ('Friday') 
then avg(sum(s.amount_sold)) over (order by t.time_id 
groups between 1 preceding and 2 following) 
else 
avg(sum(s.amount_sold)) over (order by t.time_id 
groups between 1 preceding and 1 following )
end as centered_3days_avg

from sh.sales s 
join sh.times t on t.time_id = s.time_id 
where t.calendar_week_number in(48, 49, 50, 51, 52)
and t.calendar_year = 1999
group by t.calendar_week_number , 
t.time_id
order by t.time_id ) as ttt
		where
			calendar_week_number in(49, 50, 51);















