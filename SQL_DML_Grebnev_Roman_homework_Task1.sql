
--1. Choose your top-3 favorite movies and add them to 'film' table. 

--Fill rental rates with 4.99, 9.99 and 19.99 and rental durations with 1, 2 and 3 
--weeks respectively.
--insert
--	into
--	film(title,
--	description,
--	release_year,
--	language_id,
--	rental_duration,
--	rental_rate,
--	length,
--	replacement_cost,
--	rating)
--values('Men in black',
--'Epic frim',
--1997,
--1,
--1,
--4.99,
--98,
--15.99,
--'PG-13'),
--('Yes man',
--'Turbo frim',
--2008,
--2,
--9.99,
--98,
--15.99,
--'PG-13'),
--('Inception',
--'Mega film',
--2010,
--1,
--3,
--19.99,
--120,
--21.99,
--'PG-13');

--2. Add actors who play leading roles in your favorite movies to 'actor' and 'film_actor' tables (6 or more actors in total).

--insert
--	into
--	actor (first_name,
--	last_name)
--values('WILL',
--'SMITH'),
--('TOMMY',
--'LEE JONES'),
--('LEONARDO',
--'DICAPRIO'),
--('ZOOEY',
--'DESCHANEL'),
--('TOM',
--'HARDY'),
--('JIM',
--'CARREY');
--insert
--	into
--	film_actor (actor_id,
--	film_id)
--values((
--select
--	a.actor_id
--from
--	actor a
--where
--	a.first_name like 'LEONARDO'
--	and a.last_name like 'DICAPRIO'),
--(
--select
--	f.film_id
--from
--	film f
--where
--	f.title like 'Inception'));
--
--insert
--	into
--	film_actor (actor_id,
--	film_id)
--values((
--select
--	a.actor_id
--from
--	actor a
--where
--	a.first_name like 'TOM'
--	and a.last_name like 'HARDY'),
--(
--select
--	f.film_id
--from
--	film f
--where
--	f.title like 'Inception'));
--
--insert
--	into
--	film_actor (actor_id,
--	film_id)
--values((
--select
--	a.actor_id
--from
--	actor a
--where
--	a.first_name like 'WILL'
--	and a.last_name like 'SMITH'),
--(
--select
--	f.film_id
--from
--	film f
--where
--	f.title like 'Men in black'));
--
--insert
--	into
--	film_actor (actor_id,
--	film_id)
--values((
--select
--	a.actor_id
--from
--	actor a
--where
--	a.first_name like 'TOMMY'
--	and a.last_name like 'LEE JONES'),
--(
--select
--	f.film_id
--from
--	film f
--where
--	f.title like 'Men in black'));
--
--insert
--	into
--	film_actor (actor_id,
--	film_id)
--values((
--select
--	a.actor_id
--from
--	actor a
--where
--	a.first_name like 'ZOOEY'
--	and a.last_name like 'DESCHANEL'),
--(
--select
--	f.film_id
--from
--	film f
--where
--	f.title like 'Yes man'));
--
--insert
--	into
--	film_actor (actor_id,
--	film_id)
--values((
--select
--	a.actor_id
--from
--	actor a
--where
--	a.first_name like 'JIM'
--	and a.last_name like 'CARREY'),
--(
--select
--	f.film_id
--from
--	film f
--where
--	f.title like 'Yes man'));

--3. Add your favorite movies to any store's inventory.
--insert into inventory (film_id,store_id)
--values((select f.film_id from film f
--where f.title like 'Yes man'),2);
--
--insert into inventory (film_id,store_id)
--values((select f.film_id from film f
--where f.title like 'Men in black'),2);
--
--insert into inventory (film_id,store_id)
--values((select f.film_id from film f
--where f.title like 'Men in black'),1);
--
--insert into inventory (film_id,store_id)
--values((select f.film_id from film f
--where f.title like 'Inception'),2);
--4. Alter any existing customer in the database who has at least 43 
--rental and 43 payment records. Change his/her personal data to yours (first name, 
--last name, address, etc.). Do not perform any updates on 'address' table, as it 
--can impact multiple records with the same address. Change 
--customer's create_date value to current_date

--update customer 
--set first_name = 'ROMAN',
--last_name = 'GREBNEV',
--email = 'roman.gold153@gmail.com',
--create_date = current_date 
--where customer_id = 280
--and exists(select c.customer_id , count(r.rental_id) from customer c
--join rental r on c.customer_id = r.customer_id
--group by c.customer_id 
--having count(r.rental_id) > 43 );

--5.Remove any records related to you (as a customer) from all tables except 'Customer' and 'Inventory'
--delete from payment 
--where customer_id in(select customer_id from rental
--where customer_id in(select customer_id from customer c
--where c.first_name like 'ROMAN'
--and c.last_name like 'GREBNEV'));
--
--delete from rental 
--where customer_id in(select customer_id from customer c
--where c.first_name like 'ROMAN'
--and c.last_name like 'GREBNEV');

--6.Rent you favorite movies from the store they are in and pay for them 
--(add corresponding records to the database to represent this activity)

insert
	into
	rental (rental_date ,
	inventory_id,
	customer_id,
	staff_id)
values 
(current_date,
(
select
	distinct i.inventory_id
from
	inventory i
where
	exists (
	select
		*
	from
		film f
	where
		f.title like 'Men In Black'
		and i.film_id = f.film_id)
order by
	i.inventory_id desc
limit 1),
(
select
	c.customer_id
from
	customer c
where
	c.first_name like 'ROMAN'
	and c.last_name like 'GREBNEV'),
2);

insert
	into
	rental (rental_date ,
	inventory_id,
	customer_id,
	staff_id)
values 
(current_date,
(
select
	distinct i.inventory_id
from
	inventory i
where
	exists (
	select
		*
	from
		film f
	where
		f.title like 'Yes man'
		and i.film_id = f.film_id)
order by
	i.inventory_id desc
limit 1),
(
select
	c.customer_id
from
	customer c
where
	c.first_name like 'ROMAN'
	and c.last_name like 'GREBNEV'),
2);

insert
	into
	rental (rental_date ,
	inventory_id,
	customer_id,
	staff_id)
values 
(current_date,
(
select
	distinct i.inventory_id
from
	inventory i
where
	exists (
	select
		*
	from
		film f
	where
		f.title like 'Inception'
		and i.film_id = f.film_id)
order by
	i.inventory_id desc
limit 1),
(
select
	c.customer_id
from
	customer c
where
	c.first_name like 'ROMAN'
	and c.last_name like 'GREBNEV'),
2);

insert
	into
	payment (customer_id,
	staff_id,
	rental_id,
	amount,
	payment_date)
values((
select
	c.customer_id
from
	customer c
where
	c.first_name like 'ROMAN'
	and c.last_name like 'GREBNEV' ),
(
select
	distinct staff_id
from
	rental r
where
	exists(
	select
		*
	from
		customer c2
	where
		c2.customer_id = r.customer_id
		and c2.first_name like 'ROMAN'
		and c2.last_name = 'GREBNEV');

),
(
select
	i.inventory_id
from
	inventory i
where
	exists(
	select
		*
	from
		rental r
	where
		r.inventory_id = i.inventory_id
		and exists(
		select
			*
		from
			customer c
		where
			c.customer_id = r.customer_id
			and c.first_name like 'ROMAN'
			and c.last_name like 'GREBNEV'))
	and exists(
	select
		film_id
	from
		film f
	where
		f.film_id = i.film_id
		and f.title like 'Yes man')),
select 
	extract (day
from
	(r.return_date - r.rental_date ) )*(
	select
		f.rental_rate / f.rental_duration
	from
		film f
	where
		f.title like 'Yes man'
		)
from
	rental r
where
	exists(
	select
		*
	from
		customer c
	where
		c.customer_id = r.customer_id
		and c.first_name like 'ROMAN'
		and c.last_name like 'GREBNEV')
	and exists (
	select
		*
	from
		inventory i2
	where
		i2.inventory_id = r.inventory_id
		and exists(
		select
			*
		from
			film f
		where
			f.film_id = i2.film_id
			and f.title like 'Yes man')
		)
		,
	'2017-03-01 20:38:40');

insert
	into
	payment (customer_id,
	staff_id,
	rental_id,
	amount,
	payment_date)
values((
select
	c.customer_id
from
	customer c
where
	c.first_name like 'ROMAN'
	and c.last_name like 'GREBNEV' ),
(
select
	distinct staff_id
from
	rental r
where
	exists(
	select
		*
	from
		customer c2
	where
		c2.customer_id = r.customer_id
		and c2.first_name like 'ROMAN'
		and c2.last_name = 'GREBNEV');

),
(
select
	i.inventory_id
from
	inventory i
where
	exists(
	select
		*
	from
		rental r
	where
		r.inventory_id = i.inventory_id
		and exists(
		select
			*
		from
			customer c
		where
			c.customer_id = r.customer_id
			and c.first_name like 'ROMAN'
			and c.last_name like 'GREBNEV'))
	and exists(
	select
		film_id
	from
		film f
	where
		f.film_id = i.film_id
		and f.title like 'Inception')),
select 
	extract (day
from
	(r.return_date - r.rental_date ) )*(
	select
		f.rental_rate / f.rental_duration
	from
		film f
	where
		f.title like 'Inception'
		)
from
	rental r
where
	exists(
	select
		*
	from
		customer c
	where
		c.customer_id = r.customer_id
		and c.first_name like 'ROMAN'
		and c.last_name like 'GREBNEV')
	and exists (
	select
		*
	from
		inventory i2
	where
		i2.inventory_id = r.inventory_id
		and exists(
		select
			*
		from
			film f
		where
			f.film_id = i2.film_id
			and f.title like 'Inception')
		)
		,
	'2017-03-01 20:38:40');

insert
	into
	payment (customer_id,
	staff_id,
	rental_id,
	amount,
	payment_date)
values((
select
	c.customer_id
from
	customer c
where
	c.first_name like 'ROMAN'
	and c.last_name like 'GREBNEV' ),
(
select
	distinct staff_id
from
	rental r
where
	exists(
	select
		*
	from
		customer c2
	where
		c2.customer_id = r.customer_id
		and c2.first_name like 'ROMAN'
		and c2.last_name = 'GREBNEV');

),
(
select
	i.inventory_id
from
	inventory i
where
	exists(
	select
		*
	from
		rental r
	where
		r.inventory_id = i.inventory_id
		and exists(
		select
			*
		from
			customer c
		where
			c.customer_id = r.customer_id
			and c.first_name like 'ROMAN'
			and c.last_name like 'GREBNEV'))
	and exists(
	select
		film_id
	from
		film f
	where
		f.film_id = i.film_id
		and f.title like 'Men In Black')),
select 
	extract (day
from
	(r.return_date - r.rental_date ) )*(
	select
		f.rental_rate / f.rental_duration
	from
		film f
	where
		f.title like 'Men In Black'
		)
from
	rental r
where
	exists(
	select
		*
	from
		customer c
	where
		c.customer_id = r.customer_id
		and c.first_name like 'ROMAN'
		and c.last_name like 'GREBNEV')
	and exists (
	select
		*
	from
		inventory i2
	where
		i2.inventory_id = r.inventory_id
		and exists(
		select
			*
		from
			film f
		where
			f.film_id = i2.film_id
			and f.title like 'Men In Black')
		)
		,
	'2017-03-01 20:38:40');


