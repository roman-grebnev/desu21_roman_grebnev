--view for show count of selling products by customer gender
create view sh.count_for_gender as 
select  c.cust_gender , count(s.prod_id)   from sh.customers c 
join sh.sales s on s.cust_id = c.cust_id 
join sh.channels c2 on c2.channel_id = s.channel_id 
group by c.cust_gender 
order by count(s.prod_id) desc;

--show maximum of one-off sell at channel
create view max_from_channel as
select c.channel_id ,  max(s.amount_sold)  from sh.channels c 
left join sh.sales s on s.channel_id = c.channel_id
group by c.channel_id
order by max(s.amount_sold) desc;

-- show show count of selling products by month 
create view sh.sale_of_prod_for_month as
select t.fiscal_month_name , count(s.prod_id)  from sh.times t 
join sh.sales s on s.time_id = t.time_id
group by t.fiscal_month_name ;



