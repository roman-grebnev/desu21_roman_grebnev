select
	calendar_year ,
	calendar_quarter_desc ,
	prod_category ,
	to_char(sales , '999,999.00') as "sales$" ,
	case when calendar_quarter_desc like '%01' then 'N/A'
	else to_char((sales - first_quarter_sales )/first_quarter_sales*100 , '99999.99%') end 
	 as prev ,
	cum_sum as "cum_sum$"
from
	(
	select
		t.calendar_year 
	,
		t.calendar_quarter_desc 
	,
		p.prod_category 
	,
		sum(s.amount_sold) as sales
	,
		first_value (sum(amount_sold)) over (partition by t.calendar_year, p.prod_category
                                                 order by t.calendar_quarter_desc) AS first_quarter_sales
	,
		sum(sum(amount_sold)) over(partition by t.calendar_year
	order by
		t.calendar_quarter_desc 
	range between unbounded preceding and current row ) as cum_sum
	from
		sh.sales s
	join sh.times t on
		t.time_id = s.time_id
	join sh.products p on
		p.prod_id = s.prod_id
	join sh.channels c on
		c.channel_id = s.channel_id
	where
		(p.prod_category like 'Electronics'
			or p.prod_category like 'Hardware'
			or p.prod_category like 'Software/Other')
		and c.channel_desc like 'Partners'
		or (p.prod_category like 'Electronics'
			or p.prod_category like 'Hardware'
			or p.prod_category like 'Software/Other')
		and c.channel_desc like 'Internet'
	group by
		t.calendar_year 
	,
		t.calendar_quarter_desc 
	,
		p.prod_category ) as t
where
	calendar_year in(1999, 2000)
order by calendar_year, calendar_quarter_desc ,  sales desc;

